// Inital app
const electron = require("electron");
const { autoUpdater } = require("electron-updater");
const { app, BrowserWindow, ipcMain, Menu, dialog } = require("electron");
const log = require("electron-log");

log.info("Hello, log");

let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
        },
    });
    mainWindow.loadFile("index.html");
    mainWindow.on("closed", function () {
        mainWindow = null;
    });

    // Create the menu template
    const template = [
        {
            label: "Update Custom Menu",
            click() {
                sendStatusToWindow("Checking for updates...");
                autoUpdater.checkForUpdates();
            },
        },
    ];

    // Add the menu to the main application menu
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
}

app.on("ready", () => {
    createWindow();
});

app.on("window-all-closed", function () {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", function () {
    if (mainWindow === null) {
        createWindow();
    }
});

ipcMain.on("app_version", (event) => {
    sendStatusToWindow("Getting App Version....");
    event.sender.send("app_version", { version: app.getVersion() });
});

///////////////////
// Auto upadater //
///////////////////
autoUpdater.requestHeaders = { "PRIVATE-TOKEN": QWERTYUIOP };

autoUpdater.autoDownload = true;

console.log(autoUpdater.requestHeaders);


autoUpdater.setFeedURL(
    "https://gitlab.com/api/v4/projects/46449846/jobs/artifacts/master/raw/dist/?job=build"
);

autoUpdater.on("checking-for-update", function () {
    sendStatusToWindow("Checking for update...");
});

autoUpdater.on("update-not-available", function (info) {
    sendStatusToWindow("Update not available.");
    dialog.showMessageBox(mainWindow, {
        type: "info",
        buttons: ["OK"],
        message: "No updates are currently available."
    });
});


autoUpdater.on("update-not-available", function (info) {
    sendStatusToWindow("Update not available.");
    dialog.showMessageBox(mainWindow, {
        type: "info",
        buttons: ["OK"],
        message: "No updates are currently available."
    });
});

autoUpdater.on("error", function (err) {
    sendStatusToWindow("Error in auto-updater.");
});

autoUpdater.on("download-progress", function (progressObj) {
    let log_message = "Download speed: " + progressObj.bytesPerSecond;
    log_message =
        log_message + " - Downloaded " + parseInt(progressObj.percent) + "%";
    log_message =
        log_message +
        " (" +
        progressObj.transferred +
        "/" +
        progressObj.total +
        ")";
    sendStatusToWindow(log_message);
});

autoUpdater.on("update-downloaded", function (info) {
    dialog
        .showMessageBox(mainWindow, {
            type: "question",
            buttons: ["Install", "Later"],
            defaultId: 0,
            message: "A new version of the app has been downloaded. Install now?",
        })
        .then((result) => {
            if (result.response === 0) {
                sendStatusToWindow("Installing update...");
                autoUpdater.quitAndInstall();
            } else {
                sendStatusToWindow("Update postponed.");
            }
        })
        .catch((err) => {
            sendStatusToWindow("Update error: " + err.message);
        });
});

autoUpdater.checkForUpdates();

function sendStatusToWindow(message) {
    console.log(message);
}